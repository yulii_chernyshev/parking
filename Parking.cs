using System;
using System.Collections.Generic;
using System.Linq;
using System.Timers;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;


namespace MyParking
{
    public class Parking
    {
        private double balance = Settings.defaultBalance;
        private Timer timer = new Timer(60000);
        public List<Vehicle> vehicles = new List<Vehicle>();
        public List<Transaction> Transactions = new List<Transaction>();
        
        private static Lazy<Parking> parking { get; set; } = new Lazy<Parking>(() => new Parking());

        private class TransactionLog
        {
            public string Date { get; set; }
            public double Sum { get; set; }
        }

        private object locker = new object();

        public static Parking Instance
        {
            get { return parking.Value; }
        }

      
        private Parking()
        {
            balance = Settings.defaultBalance;
            vehicles.Capacity = Settings.defaultPlaces;          
            timer.Elapsed += Timer_Elapsed;
            timer.Start();
            
        }

        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
           
            List<TransactionLog> transactions = new List<TransactionLog>();
            if (File.Exists("Transaction.log"))
            {
                transactions = JsonConvert.DeserializeObject<List<TransactionLog>>(File.ReadAllText("Transaction.log"));
                File.Delete("Transaction.log");
            }
            double sum = Transactions.Where(tra => tra.DateTime.AddMinutes(1) >= DateTime.Now).Sum(tra => tra.WriteOff);
            transactions.Add(new TransactionLog() { Sum = sum, Date = DateTime.Now.ToString() });
            string newJson = JsonConvert.SerializeObject(transactions);
            File.WriteAllText("Transaction.log", newJson);


            //using (FileStream fstream = new FileStream(@"Transaction2.log", FileMode.OpenOrCreate))
            //{               
            //   foreach (var item in transactions)
            //   {
            //        byte[] input = Encoding.Default.GetBytes(item.Date.ToString() + " Сумма:" + item.Sum + " ;");
            //        fstream.Write(input, 0, input.Length);

            //   }

            //}

            //throw new NotImplementedException();
        }

        public double Balance
        {
            get
            {
                lock (locker)
                {
                    return parking.Value.balance;
                }
            }
            set
            {
                lock (locker)
                {
                    if (value > 0)
                        parking.Value.balance = value;
                    else
                        throw new Exception("Баланс не может быть меньше нуля!");
                }
            }
        }

        public void ShowAllTransaction()
        {
            if (File.Exists("Transaction.log"))
            {
                List<TransactionLog> transactions = JsonConvert.DeserializeObject<List<TransactionLog>>(File.ReadAllText("Transaction.log"));
                transactions.ForEach((tr) => {                   
                    Console.WriteLine($"| Дата: {tr.Date.ToString()}\n| Сумма:  {tr.Sum}");                 
                });
            }
            else
                Console.WriteLine("\nФайл Transaction.log  не cоздан!");
        }
        public void ShowTransactions60sec()
        {
            Transactions.OrderBy(tr => tr.DateTime).Where(tr => tr.DateTime.AddMinutes(1) >= DateTime.Now).ToList<Transaction>().ForEach((transaction) => {          
                Console.WriteLine($"| Дата: {transaction.DateTime.ToString()}\n| Тип транс.ср.: {transaction.VehNum}\n| Снятая сумма: {transaction.WriteOff}");              
            });
        }

    }
}