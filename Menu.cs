using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyParking
{
    public class Menu
    {
        static Parking parking = Parking.Instance;
       // static Parking parking2 = new Parking();

        public static void Start()
        {
            ConsoleKeyInfo key;
            do
            {
                Console.Clear();
                ShowMainMenu();
                key = Console.ReadKey();
                if (!Char.IsDigit(key.KeyChar))
                    Console.WriteLine("\nУправление только цифрами!");
                else
                {
                    Console.Clear();
                    switch (key.KeyChar)
                    {
                        case '1': ShowBalance() ; break;
                        case '2': PlacesInfo(); break;
                        case '3': ShowTransactions60(); break;
                        case '4': ShowLogFile(); break;
                        case '5': ShowVehicles(); break;
                        case '6': AddVehicle(); break;
                        case '7': DeleteVehicle(); break;
                        case '8': TopUpBalace(); break;
                        
                    }
                    Console.WriteLine("\nДля выхода в меню нажмите на любую клавишу!");
                    Console.ReadKey();
                }


            } while (key.KeyChar != '0');

        }



        private static void ShowMainMenu()
        {
            Console.Write("\nМеню:\n1) Текущий баланс Паркинга\n" +
                "2) Узнать количество свободных/занятых мест на парковке\n" +                
                "3) Показать историю транзакций за последнюю минуту\n" +
                "4) Показать всю историю транзакций\n" +
                "5) Показать все транспортные средства на парковке\n" +
                "6) Поставить транспортное средство \n" +
                "7) Забрать транспортное средство \n" +
                "8) Пополнить баланс транспортного средства \n" +
                "0) Выход из программы\nПросто нажмите нужную цифру: ");
        }


        private static void ShowBalance()
        {
            Console.WriteLine("Текущий баланс парковки -> " + parking.Balance);
        }

        private static void PlacesInfo()
        {
            Console.WriteLine("Всего -> " + parking.vehicles.Capacity + " мест на Паркинге");
            Console.WriteLine("Свободных мест -> " + (parking.vehicles.Capacity - parking.vehicles.Count));
            Console.WriteLine("Занятых мест -> " + parking.vehicles.Count);
        }
        private static void ShowTransactions60()
        {
            parking.ShowTransactions60sec();
        }

        private static void ShowLogFile()
        {
            parking.ShowAllTransaction();
        }
        private static void ShowVehicles()
        {
            Console.WriteLine("Транспортные средства на паркове в данный момент");
            Console.WriteLine("Номер\t Тип\t Баланс");
            foreach (Vehicle veh in parking.vehicles)
            {
                Console.WriteLine(veh.ShowID() + "  " + veh.VehicleType + " " + veh.Balance);
            }
        }


        private static Vehicle SelectVehicle()
        {
            Console.WriteLine();
            foreach (Vehicle veh in parking.vehicles)
            {
                Console.WriteLine(veh.ShowID() + "  " + veh.VehicleType + " " + veh.Balance);
            }
            Console.Write("\nВведите номер транс.ср.:");
            string number = Console.ReadLine();
            Vehicle vehicle = parking.vehicles.Find(veh => veh.VehicleNum == number);
            if (vehicle == null)
                Console.WriteLine("Транс.ср. не было найдено!");
            return vehicle;
        }

        private static void AddVehicle()
        {
            if (parking.vehicles.Count < parking.vehicles.Capacity)
            {
                try
                {
                    Console.Write("Введите номер Транспортного средства: ");
                    string VegID = Console.ReadLine();
                    Console.Write("Введите баланс Транспортного средства: ");
                    double vehicleBalance = double.Parse(Console.ReadLine());
                    int Vehicletype;
                    do
                    {
                        Console.Write("Выберите тип транс.ср.:\n\t1)Car\n\t2)Truck\n\t3)Bus\n\t4)MotorBike\nВведите нужное число: ");
                        Vehicletype = int.Parse(Console.ReadLine());
                    } while (Vehicletype < 0 || Vehicletype > 4);
                    Vehicle vehicle = new Vehicle(VegID, vehicleBalance, (VehicleType)Vehicletype - 1);
                    vehicle.AddToParking(parking);
                }
                catch (FormatException)
                {
                    Console.WriteLine("Ввод некорректный! Баланс и тип транс. ср. необходимо вводить числами!");
                }
                catch (Exception ex)
                {

                    Console.WriteLine(ex.Message);
                }
            }
            else
            {
                Console.WriteLine("Нет свободных мест! ");
                return;
            }
        }
        private static void DeleteVehicle()
        {
            Console.Write("Вы вошли в режим удаления авто!");
            if (parking.vehicles.Count == 0)
                Console.WriteLine("\nАвтомобилей в паркинге нет!");
            else
            {
                Vehicle vehicle = SelectVehicle();
                if (vehicle != null)
                    vehicle.RemoveFromParking(parking);
            }
        }
        private static void TopUpBalace()
        {
            Console.WriteLine("Вы вошли в режим пополнения баланса Транс. ср.!");
            if (parking.vehicles.Count == 0)
                Console.WriteLine("\nТранс. ср. в паркинге нет!");
            else
            {
                try
                {
                    Vehicle veh = SelectVehicle();
                    if (veh != null)
                    {
                        Console.WriteLine($"Текущий баланс {veh.VehicleNum} = {veh.Balance}.");
                        Console.Write("Введите сумму для пополнения баланса:");
                        double charge = double.Parse(Console.ReadLine());
                        if (charge < 0)
                            Console.WriteLine("Вы не можете уменьшить баланс!");
                        else
                        {
                            
                            veh.Balance += charge;
                            Console.WriteLine($"Баланс после пополнения = {veh.Balance}.");
                        }
                    }
                }
                catch (FormatException)
                {
                    Console.WriteLine("Некорректный ввод! Ожидалось число!");
                }
            }
        }
    }
}